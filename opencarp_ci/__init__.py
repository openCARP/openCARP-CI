__title__ = 'openCARP-CI'
__version__ = '1.0'
__author__ = 'Jochen Klar'
__email__ = 'mail@jochenklar.de'
__license__ = 'Apache-2.0'
__copyright__ = 'Copyright (c) 2020 Karlsruhe Institute for Technology (KIT)'

VERSION = __version__
