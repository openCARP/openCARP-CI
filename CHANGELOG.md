# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## Unreleased

### Changed
- Fix `false` to `False` in `prepare_radar.py`
- Calling citeproc with `--citeproc instead` of as a filter

### Fixed
- Correctly parse `@type` when converting to RADAR metadata
- Remove `https://spdx.org/licenses/` from license name for RADAR and CFF

## v1.5.2

### Changed
- Fix "publishers" field in RADAR metadata

### Added
- Activate RADAR CI jobs

## v1.5.1

### Added
- thumbnails in the docstring pipeline are now automatically generated
- added the CI pipelines
- intialise the CHANGELOG.md
